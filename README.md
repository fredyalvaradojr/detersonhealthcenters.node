# Deterson Health Records API Server

This API was designed to just illustrate the awesome ability of Node to create a basic API. It calls a realtime database to serve the results needed for the application.

> The Production version is currently hosted at Heroku: https://det-helth-ui.herokuapp.com/doctors

## Install the API Server

#### Step 1: Download and Install Node

```bash
  * https://nodejs.org/en/download/current/
  * run installer
```

#### Step 2: Install DetersonHealthCenters.node API Server app

```bash
  * git clone https://fredyalvaradojr@bitbucket.org/fredyalvaradojr/detersonhealthcenters.node.git
  * cd detersonhealthcenters.node
  * npm install
  * npm start
```

#### Step 3: Start API Server

```bash
  * npm start
```

#### Step 4: Test doctors API Endpoint

```bash
  * http://localhost:3001/doctors
```
