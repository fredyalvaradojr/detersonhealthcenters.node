const axios = require('axios');

const getDoctors = async () => {
  return await axios
    .get('https://det-helth-ui.firebaseio.com/results.json')
    .then(res => res.data)
    .catch(err => err);
};

module.exports = getDoctors;
