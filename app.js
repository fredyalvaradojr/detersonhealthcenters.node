const express = require('express');
const app = express();
const doctors = require('./routes/doctors');

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.use('/doctors', doctors);

module.exports = app;
