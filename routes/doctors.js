const express = require('express');
const router = express.Router();
const getDoctors = require('../utils/getDoctors');

router.get('/', (req, res, next) => {
  getDoctors().then(result =>
    res.status(200).json({
      results: result,
    }),
  );
});

module.exports = router;
